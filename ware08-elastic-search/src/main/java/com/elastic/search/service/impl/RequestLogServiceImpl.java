package com.elastic.search.service.impl;

import com.elastic.search.model.RequestLog;
import com.elastic.search.repository.RequestLogRepository;
import com.elastic.search.service.RequestLogService;
import com.elastic.search.util.DateUtil;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class RequestLogServiceImpl implements RequestLogService {
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;    //es工具
    @Resource
    private RequestLogRepository requestLogRepository ;

    @Override
    public String esInsert(Integer num) {
        for (int i = 0 ; i < num ; i++){
            RequestLog requestLog = new RequestLog() ;
            requestLog.setId(System.currentTimeMillis());
            requestLog.setOrderNo(DateUtil.formatDate(new Date(),DateUtil.DATE_FORMAT_02)+System.currentTimeMillis());
            requestLog.setUserId("userId"+i);
            requestLog.setUserName("张三"+i);
            requestLog.setCreateTime(DateUtil.formatDate(new Date(),DateUtil.DATE_FORMAT_01));
            requestLogRepository.save(requestLog) ;
        }
        return "success" ;
    }

    @Override
    public Iterable<RequestLog> esFindAll (){
        return requestLogRepository.findAll() ;
    }

    @Override
    public String esUpdateById(RequestLog requestLog) {
        requestLogRepository.save(requestLog);
        return "success" ;
    }

    @Override
    public Optional<RequestLog> esSelectById(Long id) {
        return requestLogRepository.findById(id) ;
    }

    @Override
    public Iterable<RequestLog> esFindOrder() {
        // 用户名倒序
        // Sort sort = new Sort(Sort.Direction.DESC,"userName.keyword") ;
        // 创建时间正序
        Sort sort = new Sort(Sort.Direction.ASC,"createTime.keyword") ;
        return requestLogRepository.findAll(sort) ;
    }

    @Override
    public Iterable<RequestLog> esFindOrders() {
        List<Sort.Order> sortList = new ArrayList<>() ;
        Sort.Order sort1 = new Sort.Order(Sort.Direction.ASC,"createTime.keyword") ;
        Sort.Order sort2 = new Sort.Order(Sort.Direction.DESC,"userName.keyword") ;
        sortList.add(sort1) ;
        sortList.add(sort2) ;
        Sort orders = Sort.by(sortList) ;
        return requestLogRepository.findAll(orders) ;
    }

    @Override
    public Iterable<RequestLog> search() {
        // 全文搜索关键字
        /*
        String queryString="张三";
        QueryStringQueryBuilder builder = new QueryStringQueryBuilder(queryString);
        requestLogRepository.search(builder) ;
        */

        /*
         * 多条件查询
         */
         /*QueryBuilder builder = QueryBuilders.boolQuery()
                // .must(QueryBuilders.matchQuery("userName.keyword", "历张")) 搜索不到
                .must(QueryBuilders.matchQuery("userName", "张三")) // 可以搜索
                .must(QueryBuilders.matchQuery("orderNo", "20190613736278243"));
        return requestLogRepository.search(builder) ;*/
        //创建Pageable对象
        Pageable pageable = PageRequest.of(0, 3,Sort.by(Sort.Direction.ASC,"id"));
        //高亮拼接的前缀
        String preTags="<font color=\"red\">";
        //高亮拼接的后缀
        String postTags="</font>";
        //查询内容的字体
        String content = "张三";
        //查询具体的字段
        String fieldNames[]= {"userName"};
        //创建queryBuilder查询条件
        QueryBuilder queryBuilder = QueryBuilders.multiMatchQuery(content, fieldNames);
        //创建search对象
        SearchQuery query = new NativeSearchQueryBuilder().withQuery(queryBuilder).withHighlightFields(
                new HighlightBuilder.Field(fieldNames[0]).preTags(preTags).postTags(postTags)
        ).withPageable(pageable).build();
        //执行分页查询
        AggregatedPage<RequestLog> pageInfo = elasticsearchTemplate.queryForPage(query, RequestLog.class,
                new SearchResultMapper() {
                    @Override
                    public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> clazz, Pageable pageable) {
                        //定义查询出来内容存储的集合
                        List<RequestLog> content = new ArrayList<>();
                        //获取高亮的结果
                        SearchHits searchHits = response.getHits();
                        if(searchHits!=null) {
                            //获取高亮中所有的内容
                            SearchHit[] hits = searchHits.getHits();
                            if(hits.length > 0) {
                                for (SearchHit hit : hits) {
                                    RequestLog article = new RequestLog();
                                    //高亮结果的id值
                                    String id = hit.getId();
                                    //存入实体类
                                    article.setId(Long.parseLong(id));
                                    //获取第一个字段的高亮内容
                                    HighlightField highlightField1 = hit.getHighlightFields().get(fieldNames[0]);
                                    if(highlightField1 != null) {
                                        //获取第一个字段的值并封装给实体类
                                        String hight_value1 = highlightField1.getFragments()[0].toString();
                                        article.setUserName(hight_value1);
                                    }else {
                                        //获取原始的值
                                        String value = (String) hit.getSourceAsMap().get(fieldNames[0]);
                                        article.setUserName(value);
                                    }
                                   /* HighlightField highlightField2 = hit.getHighlightFields().get(fieldNames[1]);
                                    if(highlightField2 != null) {
                                        //获取第二个字段的值并封装给实体类
                                        String hight_value2 = highlightField2.getFragments()[1].toString();
                                        article.setUserName(hight_value2);
                                    }else {
                                        //获取原始的值
                                        String value2 = (String) hit.getSourceAsMap().get(fieldNames[1]);
                                        article.setTitle(value2);
                                    }*/
                                    content.add(article);
                                }
                            }
                        }
                        return new AggregatedPageImpl<>((List<T>)content);

                    }
                });
        System.out.println(pageInfo+"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        List<RequestLog> list = pageInfo.getContent();
        if(list!=null) {
            list.forEach(System.out::print);
        }
return list;
    }

}
